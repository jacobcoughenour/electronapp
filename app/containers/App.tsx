import * as React from "react";
import { Switch, Route } from "react-router";
import DefaultView from "./DefaultView";

export default class App extends React.Component {
	render() {
		return (
			<Switch>
				<Route path="/" component={DefaultView} />
			</Switch>
		);
	}
}
