import * as React from "react";
import { basename } from "path";
import ExplorerTree from "../components/ExplorerTree";
import ExplorerItem from "../components/ExplorerItem";
import PanelGroup from "react-panelgroup";
import EditorContext from "../contexts/EditorContext";
import { MdClear } from "react-icons/md";

export default (props: { path: string; filter?: RegExp }) => (
	<div
		style={{
			height: "100%",
			width: "100%"
		}}
	>
		<PanelGroup direction="column">
			<div style={{ width: "100%", display: "flex", flexDirection: "column" }}>
				<div
					style={{
						background: "rgba(255,255,255,0.08)",
						padding: "8px 12px",
						fontSize: 12
					}}
				>
					OPEN FILES
				</div>
				<EditorContext.Consumer>
					{editor =>
						!!editor
							? editor.open.map((e, i) => {
									return (
										<ExplorerItem
											key={i}
											label={basename(e)}
											icon={MdClear}
											isSelected={editor.selected.indexOf(e) > -1}
											onSelected={() => editor.setFileSelected(e)}
											onClick={() => editor.closeFile(e)}
										/>
									);
							  })
							: ""
					}
				</EditorContext.Consumer>
			</div>
			<div style={{ width: "100%", display: "flex", flexDirection: "column" }}>
				<div
					style={{
						background: "rgba(255,255,255,0.08)",
						padding: "8px 12px",
						fontSize: 12
					}}
				>
					FILES
				</div>
				<ExplorerTree path={props.path} filter={props.filter} />
			</div>
		</PanelGroup>
	</div>
);
