import * as React from "react";
import { RouteComponentProps } from "react-router";
import FileExplorer from "./FileExplorer";
import EditorContext, { EditorContextState } from "../contexts/EditorContext";
import { join } from "path";
import PanelGroup, { PanelWidth } from "react-panelgroup";
import SNFFile from "../api/SNFFile";
import EditorTree from "../components/EditorTree";

export default class DefaultView extends React.Component<
	RouteComponentProps<any>,
	EditorContextState
> {
	addFileToSelected = (file: string) => {
		const selected = this.state.selected as string[];
		if (selected.indexOf(file) === -1) {
			selected.push(file);
			this.setState({ selected });
		}
	};

	removeFileFromSelected = (file: string) => {
		const selected = this.state.selected as string[];
		let index = selected.indexOf(file);
		if (index !== -1) {
			this.setState({
				selected: selected.slice(0, index).concat(selected.slice(index + 1))
			});
		}
	};

	setFileSelected = (file: string) => {
		this.setState({
			selected: [file]
		});
	};

	openFile = (file: string) => {
		const open = this.state.open as string[];
		if (open.indexOf(file) === -1) {
			open.push(file);
			this.setState({ open });
		}
		this.setFileSelected(file);
	};

	closeFile = (file: string) => {
		const open = this.state.open as string[];
		let index = open.indexOf(file);
		if (index !== -1) {
			this.setState({
				open: open.slice(0, index).concat(open.slice(index + 1))
			});
		}
		this.removeFileFromSelected(file);
	};

	readonly state = {
		selected: [],
		addFileToSelected: this.addFileToSelected,
		removeFileFromSelected: this.removeFileFromSelected,
		setFileSelected: this.setFileSelected,
		open: [],
		openFile: this.openFile,
		closeFile: this.closeFile
	};

	render() {
		return (
			<div
				style={{
					position: "absolute",
					height: "100%",
					width: "100%"
				}}
			>
				<EditorContext.Provider value={this.state}>
					<PanelGroup
						direction="row"
						panelWidths={[
							{ size: 250, resize: "dynamic" } as PanelWidth,
							{ minSize: 300, resize: "stretch" } as PanelWidth
						]}
					>
						<FileExplorer path={join(process.cwd())} />
						<div
							style={{
								display: "flex",
								flex: 1
							}}
						>
							{this.state.open.length === 0 ? (
								<div style={{ flex: 1, textAlign: "center" }}>
									Select what you want to edit
								</div>
							) : (
								this.state.open.map((filepath, key) => {
									const isSelected = this.state.selected.indexOf(filepath) > -1;
									return (
										<div
											style={{
												flex: 1,
												opacity: isSelected ? 1 : 0,
												display: isSelected ? "flex" : "none"
											}}
											key={key}
										>
											<EditorTree file={SNFFile.from(filepath)} />
										</div>
									);
								})
							)}
						</div>
					</PanelGroup>
				</EditorContext.Provider>
			</div>
		);
	}
}
