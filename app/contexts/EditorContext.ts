import * as React from "react";

export type EditorContextState = {
	selected: string[];
	addFileToSelected: (path: string) => void;
	removeFileFromSelected: (path: string) => void;
	setFileSelected: (path: string) => void;
	open: string[];
	openFile: (path: string) => void;
	closeFile: (path: string) => void;
};

export default React.createContext<EditorContextState | null>(null);
