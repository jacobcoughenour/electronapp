import * as React from "react";
import { BrowserRouter } from "react-router-dom";
import { render } from "react-dom";
import { AppContainer } from "react-hot-loader";
import { IconContext } from "react-icons";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";
import cyan from "@material-ui/core/colors/cyan";
import pink from "@material-ui/core/colors/pink";
import App from "./containers/App";
import "typeface-roboto";
import "./app.global.scss";

const theme = createMuiTheme({
	palette: {
		type: "dark",
		primary: cyan,
		secondary: pink
	}
});

function renderApp(RootApp: any) {
	render(
		<AppContainer>
			<BrowserRouter>
				<MuiThemeProvider theme={theme}>
					<IconContext.Provider value={{ style: { verticalAlign: "middle" } }}>
						<RootApp />
					</IconContext.Provider>
				</MuiThemeProvider>
			</BrowserRouter>
		</AppContainer>,
		document.getElementById("root")
	);
}
renderApp(App);

if ((module as any).hot) {
	(module as any).hot.accept("./containers/App", () =>
		renderApp(require("./containers/App").default)
	);
}
