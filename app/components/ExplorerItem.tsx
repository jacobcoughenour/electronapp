import * as React from "react";
import ButtonBase from "@material-ui/core/ButtonBase";
import CircularProgress from "@material-ui/core/CircularProgress";
import LinearProgress from "@material-ui/core/LinearProgress";

import { IconType } from "react-icons";
import { IconButton } from "@material-ui/core";

const itemHeight: number = 22;

interface ExplorerItemProps {
	label: string;
	icon: IconType;
	loading?: boolean;
	indent?: number;
	isSelected?: boolean;
	onSelected?:
		| ((event: React.MouseEvent<HTMLElement, MouseEvent>) => void)
		| undefined;
	onClick?:
		| ((event: React.MouseEvent<HTMLElement, MouseEvent>) => void)
		| undefined;
}

export default (props: ExplorerItemProps) => {
	const Icon = props.icon;
	return (
		<div
			style={{
				position: "relative",
				width: "100%",
				height: itemHeight,
				overflow: "hidden"
			}}
		>
			<ButtonBase
				style={{
					position: "absolute",
					top: 0,
					left: 0,
					width: "100%",
					height: "100%"
				}}
				onClick={props.onSelected}
			/>
			<div
				style={{
					justifyContent: "left",
					background: props.isSelected ? "rgba(0,0,0,0.15)" : "transparent"
				}}
			>
				<div
					style={{
						paddingLeft: (props.indent || 0) * 12,
						display: "flex",
						height: itemHeight,
						alignItems: "center"
					}}
				>
					{!!props.onClick ? (
						<IconButton onClick={props.onClick}>
							<Icon
								style={{
									color: "#FFFFFF",
									fontSize: 15
									// padding: "0 8px 1px 8px"
								}}
							/>
						</IconButton>
					) : (
						<Icon
							style={{
								fontSize: 15,
								padding: "0 8px 1px 8px"
							}}
						/>
					)}
					<div
						style={{
							display: "inline-block",
							fontSize: 14,
							whiteSpace: "nowrap",
							overflow: "hidden",
							textOverflow: "ellipsis"
						}}
					>
						{props.label}
					</div>
				</div>
			</div>
			{!!props.loading && (
				<LinearProgress
					style={{
						position: "absolute",
						overflow: "hidden",
						bottom: 0,
						left: 0,
						width: "100%",
						height: 1,
						zIndex: 2
					}}
				/>
			)}
		</div>
	);
};
