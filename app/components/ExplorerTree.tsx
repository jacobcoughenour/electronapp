import * as React from "react";
import { lstatSync, readdirSync } from "fs";
import { join, basename } from "path";
import EditorContext from "../contexts/EditorContext";
import ExplorerItem from "./ExplorerItem";
import { MdFolder, MdFolderOpen, MdAdd } from "react-icons/md";

export default class ExplorerTree extends React.Component<{
	path: string;
	filter?: RegExp;
}> {
	render() {
		return (
			<div
				style={{
					overflowY: "auto",
					flex: 1
				}}
			>
				<ExplorerTreeNode
					path={this.props.path}
					filter={
						this.props.filter ||
						/(^((?!\.).)*\\[a-zA-Z0-9-_\s]*$)|(^((?!\\\.).)*[a-zA-Z0-9-_\s]*\.snf$)/
					}
				/>
			</div>
		);
	}
}

enum LoadState {
	NotLoaded,
	Loading,
	Loaded
}

interface ExplorerTreeNodeProps {
	path: string;
	parent?: React.Component<ExplorerTreeNodeProps, ExplorerTreeNodeState>;
	filter: RegExp;
}

interface ExplorerTreeNodeState {
	depth: number;
	isDirectory: boolean;
	isExpanded: boolean;
	children: Array<JSX.Element>;
	childrenLoadState: LoadState;
}

class ExplorerTreeNode extends React.Component<
	ExplorerTreeNodeProps,
	ExplorerTreeNodeState
> {
	static contextType = EditorContext;

	constructor(props: Readonly<ExplorerTreeNodeProps>) {
		super(props);

		const isDir: boolean = lstatSync(props.path).isDirectory();

		this.state = {
			depth: props.parent ? props.parent.state.depth + 1 : 0,
			isDirectory: isDir,
			isExpanded: isDir && !props.parent,
			children: [],
			childrenLoadState: LoadState.NotLoaded
		};
	}

	componentDidMount() {
		const { isDirectory, isExpanded } = this.state;
		if (isDirectory && isExpanded) this.loadChildItems();
	}

	// shouldComponentUpdate(nextProps: ExplorerTreeNodeProps) {
	// 	return !nextProps.parent || nextProps.parent.state.isExpanded;
	// }

	loadChildItems() {
		if (this.state.childrenLoadState !== LoadState.NotLoaded) return;
		this.setState({ childrenLoadState: LoadState.Loading });

		const { path, filter } = this.props;
		let paths = readdirSync(path)
			.map((name: string) => join(path, name))
			.filter(e => filter.test(e))
			.sort((a: string, b: string) =>
				a.toLowerCase() > b.toLowerCase() ? 1 : -1
			);

		const files: Array<string> = [];
		const dirs: Array<string> = paths.filter(
			e => lstatSync(e).isDirectory() || !files.push(e)
		);

		this.setState({
			children: dirs
				.concat(files)
				.map((e, i) => (
					<ExplorerTreeNode parent={this} path={e} filter={filter} key={i} />
				)),
			childrenLoadState: LoadState.Loaded
		});
	}

	handleClick = () => {
		const { isDirectory, isExpanded, childrenLoadState } = this.state;

		if (isDirectory) {
			this.setState({ isExpanded: !isExpanded });
			if (!isExpanded && childrenLoadState === LoadState.NotLoaded)
				this.loadChildItems();
		} else this.context.openFile(this.props.path);
	};

	render(): JSX.Element {
		const {
			isDirectory,
			isExpanded,
			children,
			depth,
			childrenLoadState
		} = this.state;
		const { path, parent } = this.props;

		return (
			<div style={{ height: parent ? "auto" : "100%" }}>
				<EditorContext.Consumer>
					{editor => (
						<ExplorerItem
							label={basename(path)}
							icon={
								isDirectory ? (isExpanded ? MdFolderOpen : MdFolder) : MdAdd
							}
							indent={depth}
							isSelected={
								!!editor && !isDirectory && editor.selected.indexOf(path) > -1
							}
							onSelected={this.handleClick}
							loading={childrenLoadState === LoadState.Loading}
						/>
					)}
				</EditorContext.Consumer>
				<div
					style={{
						height: isExpanded ? "auto" : 0,
						overflow: "hidden"
					}}
				>
					{children}
				</div>
			</div>
		);
	}
}
