import * as React from "react";
import CircularProgress from "@material-ui/core/CircularProgress";
import ButtonBase from "@material-ui/core/ButtonBase";
import SNFFile, { LoadState, FileData, FileDataGroup } from "../api/SNFFile";
import SNValue from "../api/types/SNValue";
import { MdChevronRight } from "react-icons/md";

interface EditorTreeProps {
	file: SNFFile;
}

interface EditorTreeState {
	filedata: FileData;
	tree: any;
}

export default class EditorTree extends React.Component<
	EditorTreeProps,
	EditorTreeState
> {
	constructor(props: EditorTreeProps) {
		super(props);

		this.state = {
			filedata: {},
			tree: []
		};
	}

	_createTree(data: FileData) {
		return Object.keys(data).map((groupname, groupkey) => {
			return data[groupname] instanceof SNValue ? (
				<div key={groupkey}>
					<EditorTreeValue
						label={groupname}
						value={data[groupname] as SNValue<any>}
					/>
				</div>
			) : (
				<EditorTreeGroup
					key={groupkey}
					group={data[groupname] as FileDataGroup}
					groupname={groupname}
				/>
			);
		});
	}

	componentDidMount() {
		// fetch file data async
		this.props.file.get().then(data => {
			this.setState({
				filedata: data,
				tree: this._createTree(data)
			});

			// setTimeout(() => this.setState({ filedata: data }), 2000);
		});
	}

	componentDidUpdate(prev: EditorTreeProps) {
		console.log("update", prev.file !== this.props.file);

		// if the file prop is changed, fetch data again
		if (prev.file !== this.props.file)
			this.props.file.get().then(data => {
				this.setState({ filedata: data, tree: this._createTree(data) });
				// setTimeout(() => this.setState({ filedata: data }), 2000);
			});
	}

	render() {
		console.log("render");

		return this.props.file.state !== LoadState.Loaded ? (
			<CircularProgress
				color="primary"
				disableShrink
				style={{
					position: "absolute",
					top: "50%",
					left: "50%"
				}}
			/>
		) : (
			<div
				style={{
					overflowY: "auto",
					flex: 1
				}}
			>
				{this.state.tree}
			</div>
		);
	}
}

interface EditorTreeGroupProps {
	groupname: string;
	group: FileDataGroup;
}

interface EditorTreeGroupState {
	open: boolean;
}

class EditorTreeGroup extends React.Component<
	EditorTreeGroupProps,
	EditorTreeGroupState
> {
	constructor(props: EditorTreeGroupProps) {
		super(props);
		this.state = {
			open: false
		};
	}

	_handleClick = () => {
		this.setState({ open: !this.state.open });
	};

	render() {
		const { groupname, group } = this.props;
		const { open } = this.state;
		return (
			<>
				<ButtonBase
					style={{
						width: "100%",
						background: "rgba(255,255,255,0.1)",
						padding: "4px 6px",
						justifyContent: "start"
					}}
					onClick={this._handleClick}
				>
					<MdChevronRight
						style={{
							fontSize: 16,
							transition: "transform 0.15s",
							transform: open ? "rotate(45deg)" : "rotate(0deg)"
						}}
					/>
					<span
						style={{
							paddingLeft: 4
						}}
					>
						{groupname}
					</span>
				</ButtonBase>
				<div
					style={{
						paddingLeft: 12,
						height: open ? "" : 0,
						overflow: "hidden",
						opacity: open ? 1 : 0,
						transition: "opacity 0.15s"
					}}
				>
					{Object.keys(group).map((propname, propkey) => (
						<EditorTreeValue
							key={propkey}
							label={propname}
							value={group[propname]}
						/>
					))}
				</div>
			</>
		);
	}
}

class EditorTreeValue extends React.Component<{
	label: string;
	value: SNValue<any>;
}> {
	render() {
		// get control component class for this value
		// capitalize it to make it valid jsx
		const Control = this.props.value.control;
		return (
			<div
				style={{
					padding: 0
				}}
			>
				<Control label={this.props.label} value={this.props.value} />
			</div>
		);
	}
}
