import * as React from "react";
import SNValue from "./SNValue";

export default class Color extends SNValue<number[]> {
	control = ColorControl;

	protected deserialize(values: string[]): number[] {
		throw new Error("Method not implemented.");
	}
	serialize(): string[] {
		return [];
	}
}

class ColorControl extends React.Component {
	render() {
		return <div>color control</div>;
	}
}
