export default abstract class SNValue<T> {
	readonly path: string[];
	protected value: T;
	readonly control: any;

	constructor(path: string[], values: string[]) {
		this.path = path;
		this.value = this.deserialize(values);
	}

	/**
	 * Deserialize raw values from strings into value type T
	 * @param values raw value(s)
	 */
	protected abstract deserialize(values: string[]): T;

	/**
	 * convert value(s) back into a string
	 */
	abstract serialize(): string[];
}
