import SNValue from "./SNValue";
// import Number from "./Number";
import String from "./String";
import Array from "./Array";

/**
 * Uses the given path and value(s) to return the type for the value
 * @param path raw path split by "_"
 * @param values raw values split by ", "
 */
export function resolveType(path: string[], values: string[]): SNValue<any> {
	// return values.length === 1 ? values[0] : values;
	if (values.length > 1) {
		return new Array(path, values);
	}
	return new String(path, values);
}
