import * as React from "react";
import SNValue from "./SNValue";

export default class Number extends SNValue<number> {
	control = NumberControl;

	deserialize(values: string[]): number {
		return parseFloat(values[0]);
	}
	serialize(): string[] {
		return [this.value.toFixed(18)];
	}
}

class NumberControl extends React.Component {
	render() {
		return <div>number control</div>;
	}
}
