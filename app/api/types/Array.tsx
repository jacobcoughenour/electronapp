import * as React from "react";
import SNValue from "./SNValue";
import { resolveType } from ".";

export default class Array extends SNValue<SNValue<any>[]> {
	control = ArrayControl;

	protected deserialize(values: string[]): SNValue<any>[] {
		return values.map((value, index) =>
			resolveType([...this.path, index.toString()], [value])
		);
	}

	serialize(): string[] {
		return this.value.map(value => value.serialize()[0]);
	}
}

class ArrayControl extends React.Component<{
	label: String;
	value: Array;
}> {
	render() {
		return (
			<>
				<div
					style={{
						display: "flex"
					}}
				>
					<span style={{ flex: 1 }}>{this.props.label}</span>
					<span>{this.props.value.serialize().join(", ")}</span>
				</div>
				<div>other</div>
			</>
		);
	}
}
