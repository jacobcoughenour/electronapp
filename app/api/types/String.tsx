import * as React from "react";
import SNValue from "./SNValue";
import Input from "@material-ui/core/Input";

export default class String extends SNValue<string> {
	control = StringControl;

	deserialize(values: string[]): string {
		return values.join(", ");
	}
	serialize() {
		return [this.value];
	}
}

class StringControl extends React.Component<{
	label: String;
	value: String;
}> {
	render() {
		return (
			<div
				style={{
					display: "flex"
				}}
			>
				<span style={{ flex: 1 }}>{this.props.label}</span>
				<Input
					color="white"
					margin="dense"
					value={this.props.value.serialize()}
				/>
			</div>
		);
	}
}
