import { Parser as HtmlParser, DomHandler } from "htmlparser2";
import { createReadStream } from "fs";
import { isObject } from "util";
import { resolveType } from "./types";
import SNValue from "./types/SNValue";

export enum LoadState {
	Unloaded,
	Loading,
	Loaded,
	Saving
}

export type FileData = {
	[s: string]: FileDataGroup | SNValue<any>;
};

export type FileDataGroup = {
	[s: string]: SNValue<any>;
};

// this gives us access to htmlparser2's instance vars
interface ParserInstance extends HtmlParser {
	startIndex: any;
	endIndex: any;
}

export default class SNFFile {
	protected static _cache: Map<string, SNFFile> = new Map<string, SNFFile>();

	/**
	 * get file instance
	 * @static
	 * @param {string} path path to the file
	 * @returns {SNFFile}
	 * @memberof SNFFile
	 */
	public static from(path: string): SNFFile {
		let ent = SNFFile._cache.get(path);
		if (!ent) {
			ent = new SNFFile(path);
			SNFFile._cache.set(path, ent);
		}
		return ent;
	}

	readonly _path: string;
	private _state: LoadState;
	private readonly _parser: ParserInstance;
	private _data: FileData;

	get state() {
		return this._state;
	}

	get isLoaded() {
		return this._state === LoadState.Loaded;
	}

	protected constructor(path: string) {
		// TODO verify path

		this._path = path;
		this._state = LoadState.Unloaded;
		this._parser = (new HtmlParser(
			{
				onopentag: (name: string, tags: { [s: string]: string }) => {
					// console.log().startIndex);
					if (name === "sn_value")
						SNFFile.loadValue(
							this._data,
							tags.name.split("_"),
							tags.value.split(", ")
						);
				}
			} as DomHandler,
			{
				decodeEntities: true
			}
		) as unknown) as ParserInstance;
	}

	private static loadValue(
		parent: any,
		path: string[],
		values: string[],
		index: number = 0
	) {
		// path is resolved
		if (path.length - index === 1) {
			// set value(s)
			parent[path[index]] = resolveType(path, values);
			// path is not resolved
		} else {
			// create node if we need to
			if (!parent.hasOwnProperty(path[index])) parent[path[index]] = {};

			// handle lists
			// if next value is the end and is a digit
			// if the prop already contains a value, then we insert it into the new object
			if (
				path.length - index === 2 &&
				!isNaN(+path[index + 1]) &&
				!isObject(parent[path[index]])
			)
				parent[path[index]] = { 0: parent[path[index]] };

			// go deeper
			SNFFile.loadValue(parent[path[index]], path, values, index + 1);
		}
	}

	save() {
		console.log("save");
	}

	private _asyncload: Promise<never>;

	private load(): Promise<never> {
		if (!this._asyncload)
			this._asyncload = new Promise(resolve => {
				this._data = {};
				this._state = LoadState.Loading;

				createReadStream(this._path, { encoding: "utf8" })
					.on("data", data => {
						this._parser.write(data);
					})
					.on("close", () => {
						this._parser.end();
						this._state = LoadState.Loaded;
						resolve();
					});
			});
		return this._asyncload;
	}

	get(): Promise<FileData> {
		return this.load().then(() => {
			return this._data;
		});
	}
}
